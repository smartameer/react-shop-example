import React, { FC, Fragment, ReactElement, useEffect, useState } from 'react'
import Modal from 'react-modal'
import { FaTimes } from 'react-icons/fa'
import Button from './components/button'
import ProductList from './components/product-list'
import { Form } from './components/form'
import logo from './images/droppe-logo.png'
import img1 from './images/img1.png'
import img2 from './images/img2.png'
import styles from './shop-app.module.css'
import { IProductSubmissionPayload } from './interfaces'
import { useProductCtx } from './contexts/products'

const ShopApp:FC = ():ReactElement => {
  const {progressing, products, getProducts, addProduct} = useProductCtx()
  const [isOpen, setIsOpen] = useState<boolean>(false)
  const [message, setMessage] = useState<string|null>(null)
  const numFavorites = products.filter(product => product.isFavorite).length

  const handleAddProduct = async (addProductData: IProductSubmissionPayload) => {
		const { title, description, price } = addProductData
    setIsOpen(false)
    setMessage('Adding product...')
		try {
      addProduct({ title, description, price }).then(() => {
        setMessage(null)
      })
		} catch (error) {
      setMessage('An error occurred while adding product.')
		}
  }

  const toggleModalState = () => {
    setIsOpen(prevState => !prevState)
  }

  useEffect((): any => {
    document.title = 'Droppe refactor app'
    getProducts()
  }, [])

  return (
    <Fragment>
      <div data-testid="header" className={styles.header}>
        <div className={['container', styles.headerImageWrapper].join(' ')}>
          <img src={logo} className={styles.headerImage} alt="Droppe"/>
        </div>
      </div>

      <div
        data-testid="main"
        className={['container', styles.main, styles.mainImageWrapper].join(' ')}
      >
        <img src={img1} alt="Header1" className={styles.mainImage} />
        <img src={img2} alt="Header2" className={styles.mainImage} />
      </div>

      <div className={['container', styles.main].join(' ')} style={{paddingTop: 0}}>
        {message !== null ? (
          <div className={styles.messageContainer}>
            <i data-testid="adding-product-inprogress">{message}</i>
          </div>
        ) : (
          <div className={styles.buttonWrapper}>
            <Button
              testID="add-product-action"
              onClick={toggleModalState}
              disabled={message !== null || progressing}
            >
              Send product proposal
            </Button>
          </div>
        )}

        {products && !!products.length && (
          <Fragment>
            <div data-testid="product-status" className={styles.statsContainer}>
              <span>Total products: <span data-testid="products-count">{products.length}</span></span>
              {' - '}
              <span>Number of favorites: <span data-testid="products-favourites">{numFavorites}</span></span>
            </div>
            <ProductList products={products}/>
          </Fragment>
        )}
      </div>

      <div data-testid="footer">
        <Modal
          isOpen={isOpen}
          className={styles.reactModalContent}
          overlayClassName={styles.reactModalOverlay}
        >
          <div className={styles.modalContentHelper} data-testid="add-product-form">
            <button className={styles.modalClose} onClick={toggleModalState}><FaTimes /></button>
            <Form onSubmit={handleAddProduct} />
          </div>
        </Modal>
      </div>
    </Fragment>
  )
}

export default ShopApp
