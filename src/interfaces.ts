import { ButtonHTMLAttributes } from 'react'

export interface IRating {
  rate: number;
  count: number;
}
export interface IProduct {
  id: number,
  title: string;
  description?: string;
  price?: number;
  isFavorite?: boolean;
  rating?: IRating
}

export interface IProductProps {
  product: IProduct;
  productCtx?: IProductContext;
}

export interface IProductListProps {
  products: IProduct[];
}

export interface IFormProps {
  onSubmit: (payload: { title: string; description: string; price: number }) => void;
}

export interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement>{
  children: any;
  onClick?: (event: any) => void;
  className?: string;
  testID?: string;
}

export interface IProductSubmissionPayload {
  title: string;
  description?: string;
  price?: number;
}

export interface IProductContext {
  products: IProduct[],
  getProducts: () => Promise<IProduct[]>,
  addProduct: (product: IProductSubmissionPayload) => Promise<IProduct|null>,
  toggleFavorite: (productId: number) => void,
  progressing: boolean,
}
