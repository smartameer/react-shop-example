import React from 'react'
import { act, cleanup } from '@testing-library/react'
import { renderHook } from '@testing-library/react-hooks'

import { fetchProducts, submitProduct } from '../../services/api'
import { ProductProvider, useProductCtx } from '../products'
import { IProduct } from '../../interfaces'

jest.mock('../../services/api', () => {
  return {
    fetchProducts: jest.fn().mockResolvedValue([{
        id: 1,
        title: 'First Product',
        description: 'First Product Description',
        price: 12
      }]
    ),
    submitProduct: jest.fn().mockImplementation((data: IProduct) => {
      return Promise.resolve({
        ...data,
        id: 2
      })
    })
  }
})
// jest.mock('submitProduct', submitMock)

describe('Product Context', () => {
  afterEach(cleanup)

  it('should give initial data from context', () => {
    const wrapper = ({ children }: any) => (
      <ProductProvider>
        {children}
      </ProductProvider>
    )
    const { result } = renderHook(useProductCtx, {
      wrapper: wrapper
    })

    expect(result.current).toMatchObject({
      products: [],
      progressing: false,
      getProducts: expect.any(Function),
      addProduct: expect.any(Function),
      toggleFavorite: expect.any(Function)
    })
  })

  it('should give new data from context once getProducts is called', async () => {
    const wrapper = ({ children }: any) => (
      <ProductProvider>
        {children}
      </ProductProvider>
    )
    const { result } = renderHook(useProductCtx, {
      wrapper: wrapper
    })
    await act(async () => {
      await result.current.getProducts()
    })
    expect(result.current.products).toMatchObject([{
      id: 1,
      title: 'First Product',
      description: 'First Product Description',
      price: 12
    }])
    expect(fetchProducts).toHaveBeenCalledTimes(1)

  })

  it('should give new data from context once addProduct is called', async () => {
    const wrapper = ({ children }: any) => (
      <ProductProvider>
        {children}
      </ProductProvider>
    )
    const { result } = renderHook(useProductCtx, {
      wrapper: wrapper
    })
    await act(async () => {
      await result.current.addProduct({
        title: 'Test',
        description: 'test',
        price: 3
      })
    })
    expect(result.current.products).toMatchObject([{
      id: 2,
      title: 'Test',
      description: 'test',
      price: 3
    }])
    expect(submitProduct).toHaveBeenCalledTimes(1)
  })

  it.skip('should give updated data from context once toggleFavorite is called', async () => {
    const wrapper = ({ children }: any) => (
      <ProductProvider>
        {children}
      </ProductProvider>
    )
    const { result } = renderHook(useProductCtx, {
      wrapper: wrapper
    })
    await act(async () => {
      await result.current.getProducts()
      result.current.toggleFavorite(1)
    })
    expect(result.current.products).toMatchObject([{
      id: 1,
      title: 'First Product',
      description: 'First Product Description',
      price: 12,
      isFavourite: true
    }])
  })
})

