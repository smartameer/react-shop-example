import React, { PropsWithChildren, createContext, useContext, useMemo, useState } from 'react'
import { IProduct, IProductContext, IProductSubmissionPayload } from '../interfaces'
import { fetchProducts, submitProduct } from '../services/api'


const ProductsCtx = createContext<IProductContext | undefined>(undefined)

export const useProductCtx = () => {
  const ctx = useContext(ProductsCtx)
  if (!ctx) {
    throw new Error(
      'Opps! seems like this hook is called outside of ProductProvider!'
    )
  }
  return ctx
}

export const withProductCtx =  (Component: any) => {
  return function WrappedComponent (props: any) {
    const productCtx = useProductCtx()
    return <Component {...props} productCtx={productCtx} />
  }
}

export const ProductProvider = ({
  children
}: PropsWithChildren<{}>) => {
  const [products, setProducts] = useState<IProduct[]>([])
  const [progressing, setProgress] = useState<boolean>(false)

  const getProducts = async () => {
    setProgress(true)
    try {
      const items = await fetchProducts()
      if (items.length > 0) {
        setProducts(items)
      }
      setProgress(false)
      return items
    } catch (error) {
      console.log(error)
      console.error('Error in fetching products')
    }
    setProgress(false)
  }

  const addProduct = async (product: IProductSubmissionPayload) => {
    setProgress(true)
    try {
      const response = await submitProduct(product)
      const item: { id: number } = response
      if (item.id) {
        const newProduct = {
          ...product,
          id: item.id
        }
        setProducts(prevProducts => ([
          ...prevProducts,
          newProduct
        ]))
        setProgress(false)
        return newProduct
      }
    } catch (error) {
      console.error('Error in adding new product')
    }
    setProgress(false)
    return null
  }

  const toggleFavorite = (productId: number) => {
    setProgress(true)
    const idx = products.findIndex(product => product.id === productId)
    const updatableProduct: IProduct = {...products[idx]}
    updatableProduct.isFavorite = !updatableProduct.isFavorite

    setProducts((prevState) => ([
      ...prevState.slice(0, idx),
      updatableProduct,
      ...prevState.slice(idx + 1)
    ]))
    setProgress(false)
  }

  const contextValue = useMemo( () => ({
    products,
    progressing
  }), [products, progressing])

  return (
    <ProductsCtx.Provider value={{...contextValue, getProducts, addProduct, toggleFavorite}}>
      {children}
    </ProductsCtx.Provider>
  )
}
