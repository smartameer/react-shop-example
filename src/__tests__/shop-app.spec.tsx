import React from 'react'
import {act, cleanup, render} from '@testing-library/react'
import Modal from 'react-modal'
import userEvent from '@testing-library/user-event'

import ShopApp from '../shop-app'
import { fakeProducts, fetchMock, fetchMockProducts } from '../../mocks/mock-api'
import { ProductProvider } from '../contexts/products'

jest.useFakeTimers()

const customRender = (component: any) => {
  return render(
    <ProductProvider>{component}</ProductProvider>
  )
}

describe('Shopping App Component', () => {
  let originalFetch:any

  beforeEach(() => {
    originalFetch = (global as any).fetch
  })
  afterEach(() => {
    (global as any).fetch = originalFetch
    cleanup()
  })

  it('should render shop app main component', () => {
    const { getByTestId } = customRender(<ShopApp />)

    expect(getByTestId('header')).toBeDefined()
    expect(getByTestId('main')).toBeDefined()
    expect(getByTestId('main')).toBeDefined()
    expect(getByTestId('add-product-action')).toBeDefined()
    expect(getByTestId('add-product-action').innerHTML).toEqual('Send product proposal')
  })

  it('should render logo & images in header & main section', () => {
    const { getByTestId } = customRender(<ShopApp />)

    expect(getByTestId('header').querySelectorAll('img')).toHaveLength(1)
    expect(getByTestId('main').querySelectorAll('img')).toHaveLength(2)
  })

  it('should open modal with form on `Send product proposal` button click', () => {
    let response: any
    act(() => {
      response = customRender(<ShopApp />)
      Modal.setAppElement(response.getByTestId('footer'))
      userEvent.click(response.getByTestId('add-product-action'))
    })
    const { getByTestId } = response
    expect(getByTestId('add-product-form')).toBeDefined()
  })

  it('should call api to get products list & render', async () => {
    let response: any
    (global as any).fetch = fetchMock;
    (global as any).document = {
      title: 'Hello'
    }
    await act(async () => {
      response = customRender(<ShopApp />)
    })

    const { getByTestId, queryAllByTestId } = response

    expect(document.title).toEqual('Droppe refactor app')
    expect(fetchMock).toBeCalledTimes(1)
    expect(fetchMockProducts.json).toBeCalledTimes(1)
    expect(getByTestId('product-status')).toBeDefined()
    expect(getByTestId('products-count').innerHTML).toEqual(fakeProducts.length.toString())
    expect(getByTestId('products-favourites').innerHTML).toEqual('0')

    expect(queryAllByTestId('product-favourite-action-message')[0].innerHTML).toEqual('Add to favorites')
    userEvent.click(queryAllByTestId('product-favourite-action')[0])
    expect(queryAllByTestId('product-favourite-action-message')[0].innerHTML).toEqual('Remove from favorites')
    expect(getByTestId('products-favourites').innerHTML).toEqual('1')
  })
})
