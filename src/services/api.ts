import { IProductSubmissionPayload } from '../interfaces'

const BASE_URL = 'https://fakestoreapi.com'

export const fetchProducts = async () => {
  try {
    const response = await fetch(BASE_URL + '/products')
    return response.json()
  } catch (error) {
    console.error('Error in fetching products', error)
  }
  return []
}

export const submitProduct = async ({ title, description, price}:IProductSubmissionPayload) => {
  try {
    const response = await fetch('https://fakestoreapi.com/products', {
      method: 'POST',
      body: JSON.stringify({ title, price, description })
    })
    return response.json()
  } catch (error) {
    throw new Error('Unable to add new product')
  }
}
