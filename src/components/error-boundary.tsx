import React, { PropsWithChildren, ReactElement } from 'react'

export class ErrorBoundary extends React.Component {
  constructor (props: PropsWithChildren<null>) {
    super(props)
    this.state = { hasError: false }
  }

  static getDerivedStateFromError () {
    // Update state so the next render will show the fallback UI.
    return { hasError: true }
  }

  componentDidCatch (error: Error, errorInfo: any) {
    console.error(JSON.stringify({ msg: 'Error in Droppe App', error, errorInfo }))
  }

  render () {
    return this.props.children as ReactElement
  }
}
