import React, { FC, useState } from 'react'
import Button from './button'
import styles from './form.module.css'
import { IFormProps } from '../interfaces'


export const Form: FC<IFormProps> = ({ onSubmit }: IFormProps) => {
  const [title, setTitle] = useState<string|undefined>(undefined)
  const [price, setPrice] = useState<string|undefined>(undefined)
  const [description, setDescription] = useState<string|undefined>(undefined)
  const [errorMessage, setErrorMessage] = useState<string|undefined>(undefined)

  const handleSubmit = (e: any) => {
    e.preventDefault()
    setErrorMessage(undefined)

    if (!title) {
      setErrorMessage('Your product needs a title.')
      return
    }

    if (!description || !price) {
      setErrorMessage('Your product needs details. e.g. price, description.')
      return
    }

    onSubmit({ title, description, price: parseFloat(price) || 0 })

    setTitle('')
    setPrice('')
    setDescription('')
  }

  return (
    <form className={styles.form} data-testid="product-add-form" onSubmit={handleSubmit}>
      <label htmlFor="title" className={styles.label}>Product title: *</label>

      <input
        id="title"
        placeholder="Title..."
        value={title}
        data-testid="product-title-input"
        onChange={e => setTitle(e.target.value)}
        className={styles.input}
      />

      <label htmlFor="price" className={styles.label}>Product details: *</label>

      <input
        id="price"
        value={price}
        data-testid="product-price-input"
        onChange={e => setPrice(e.target.value)}
        placeholder="Price..."
        className={styles.input}
      />

      <textarea
        id="description"
        value={description}
        data-testid="product-description-input"
        onChange={e=> setDescription(e.target.value)}
        placeholder="Start typing product description here..."
        defaultValue=""
        className={styles.textarea}
      />

      {errorMessage && (<p data-testid="product-form-error-message" className={styles.errorMessage}>{errorMessage}</p>)}
      <Button testID="product-form-submission" onClick={handleSubmit}>Add a product</Button>
    </form>
  )
}
