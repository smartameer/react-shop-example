import React, { PureComponent } from 'react'
import { FaRegStar, FaStar } from 'react-icons/fa'
import styles from './product.module.css'
import Button from './button'
import { IProductProps } from '../interfaces'
import { withProductCtx } from '../contexts/products'

class Product extends PureComponent<IProductProps> {
  handleFavClick = () => {
    const {product, productCtx } = this.props
    const { toggleFavorite = null } = productCtx || {}

    if (toggleFavorite !== null) {
      toggleFavorite(product.id)
    }
  }

  render () {
    const { product} = this.props

    return (
      <div data-testid="product" className={styles.product}>
        <span data-testid="product-title" className={styles.productTitle} title={product.title}>{product.title}</span>

        {product.rating && (
          <p>
            <strong>Rating: <span data-testid="product-rating">{`${product.rating.rate || 0}/5`}</span></strong>
          </p>
        )}

        <p>
          <b>Price: <span data-testid="product-price">${product.price}</span></b>
        </p>

        <p className={styles.productBody}>
          <span><b>Description:</b></span>
          <br/>
          <span data-testid="product-description" title={product.description}>{product.description}</span>
        </p>

        <div className={styles.actionBar}>
          <Button
            testID="product-favourite-action"
            className={`${styles.actionBarItem} ${
              product.isFavorite ? 'active' : ''
            }`}
            onClick={this.handleFavClick}
          >
            {!product.isFavorite ? <FaRegStar /> : <FaStar/>}
            <span data-testid="product-favourite-action-message" className={styles.actionBarItemLabel}>
              {product.isFavorite ? 'Remove from favorites' : 'Add to favorites'}
            </span>
          </Button>
        </div>
      </div>
    )
  }
}

export default withProductCtx(Product)
