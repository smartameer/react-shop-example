import React from 'react'
import {cleanup, render} from '@testing-library/react'
import Button from '../button'
import userEvent from '@testing-library/user-event'

describe('Button Component', () => {
  afterEach(cleanup)

  test('should render button component', () => {
    const children = 'hello'
    const { getByTestId } = render(<Button>{children}</Button>)

    expect(getByTestId('button')).toBeDefined()
  })

  test('should render button with onClick handler', () => {
    const handleClick = jest.fn()
    const { getByTestId } = render(<Button onClick={handleClick}>Hello</Button>)

    const button = getByTestId('button')
    userEvent.click(button)

    expect(handleClick).toHaveBeenCalled()
    expect(handleClick).toHaveBeenCalledTimes(1)
  })

  test('should render button with other props along with button internal', () => {
    const children = 'Hello'
    const type = 'submit'
    const className = 'btn-default'
    const testID = 'hello-button'
    const { getByTestId } = render(<Button testID={testID} type={type} className={className}>{children}</Button>)

    const button = getByTestId(testID)
    expect(button.innerHTML).toEqual(children)
    expect(button.getAttribute('type')).toEqual(type)
    expect(button.getAttribute('class')).toEqual(className)
  })
})
