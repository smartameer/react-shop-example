import React from 'react'
import {cleanup, render} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { Form } from '../form'

describe('Button Component', () => {
  afterEach(cleanup)

  it('should render the form', () => {
    const { getByTestId } = render(<Form onSubmit={jest.fn}/>)

    expect(getByTestId('product-add-form')).toBeDefined()
    expect(getByTestId('product-title-input')).toBeDefined()
    expect(getByTestId('product-price-input')).toBeDefined()
    expect(getByTestId('product-description-input')).toBeDefined()
    expect(getByTestId('product-form-submission')).toBeDefined()
  })

  it('should show error for form submission when no values given in title', () => {
    const { getByTestId } = render(<Form onSubmit={jest.fn}/>)
    userEvent.click(getByTestId('product-form-submission'))
    expect(getByTestId('product-form-error-message')).toBeDefined()
    expect(getByTestId('product-form-error-message').innerHTML).toEqual('Your product needs a title.')
  })

  it('should show error for form submission when no values given in description & price', () => {
    const { getByTestId } = render(<Form onSubmit={jest.fn}/>)
    userEvent.type(getByTestId('product-title-input'), 'Test Product')
    userEvent.click(getByTestId('product-form-submission'))
    expect(getByTestId('product-form-error-message')).toBeDefined()
    expect(getByTestId('product-form-error-message').innerHTML)
      .toEqual('Your product needs details. e.g. price, description.')
  })

  it('should call submit props when data is valid', async () => {
    const handleSubmit = jest.fn()
    const title = 'Test Product'
    const price = 12
    const desc = 'Test Description'
    const {getByTestId} = render(<Form onSubmit={handleSubmit}/>)
    await userEvent.type(getByTestId('product-title-input'), title)
    await userEvent.type(getByTestId('product-price-input'), price.toString())
    await userEvent.type(getByTestId('product-description-input'), desc)
    userEvent.click(getByTestId('product-form-submission'))

    expect(handleSubmit).toHaveBeenCalledTimes(1)
    expect(handleSubmit).toHaveBeenCalledWith({
      title,
      description: desc,
      price
    })
  })

  it('should reset form after valid submission & remove error message', async () => {
    const handleSubmit = jest.fn()
    const title = 'Test Product'
    const price = 12
    const desc = 'Test Description'
    const {getByTestId, queryAllByTestId} = render(<Form onSubmit={handleSubmit}/>)
    await userEvent.type(getByTestId('product-title-input'), title)
    userEvent.click(getByTestId('product-form-submission'))
    expect(queryAllByTestId('product-form-error-message')).toHaveLength(1)
    await userEvent.type(getByTestId('product-price-input'), price.toString())
    await userEvent.type(getByTestId('product-description-input'), desc)

    expect(getByTestId('product-title-input').value).toEqual(title)
    expect(getByTestId('product-price-input').value).toEqual(price.toString())
    expect(getByTestId('product-description-input').value).toEqual(desc)
    userEvent.click(getByTestId('product-form-submission'))
    expect(getByTestId('product-title-input').value).toEqual('')
    expect(getByTestId('product-price-input').value).toEqual('')
    expect(getByTestId('product-description-input').value).toEqual('')
    expect(queryAllByTestId('product-form-error-message')).toHaveLength(0)
  })
})
