import React from 'react'
import {cleanup, render} from '@testing-library/react'
import ProductList from '../product-list'
import { fakeProducts } from '../../../mocks/mock-api'
import { ProductProvider } from '../../contexts/products'

const customRender = (component: any) => {
  return render(
    <ProductProvider>{component}</ProductProvider>
  )
}

describe('Product List Component', () => {
  afterEach(cleanup)

  it('should render the product list', () => {
    const { getByTestId, queryAllByTestId } = customRender(<ProductList products={fakeProducts}/>)

    expect(getByTestId('products-list')).toBeDefined()
    expect(queryAllByTestId('product')).toHaveLength(fakeProducts.length)
  })
})
