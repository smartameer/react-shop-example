import React, { ReactElement } from 'react'
import styles from './button.module.css'
import { ButtonProps } from '../interfaces'


const Button: React.FC<ButtonProps> = ({
  children,
  onClick,
  type = 'button',
  disabled = false,
  className,
  testID = 'button'
}: ButtonProps): ReactElement => (
  <button
    data-testid={testID}
    className={className || styles.button}
    disabled={disabled}
    type={type}
    onClick={onClick}
  >
    {children}
  </button>
)

export default Button
