import React, { FC } from 'react'
import lodash from 'lodash'
import Product from './product'
import { IProduct, IProductListProps } from '../interfaces'

const ProductList: FC<IProductListProps> = ({ products }: IProductListProps) => (
  <div data-testid="products-list">
    {lodash.reverse(products.map((product: IProduct) => <Product key={product.id} product={product} />))}
  </div>
)

export default ProductList
